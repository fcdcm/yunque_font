# 1.配置文件的配置

`
writer:
  plugin: "pgreader"
  url: "jdbc:postgresql://127.0.0.1:5432/test"
  username: "root"
  password: "root"
  table: "student"
  column: "id,name"
`

# 2.参数的说明

plugin: 插件的名称
url: 请求访问的路径
username：用户名
password：密码
table：写入的表
column：写入的列

| 参数名称 | 参数描述 |      |
| -------- | -------- | ---- |
|          |          |      |
|          |          |      |
|          |          |      |



# 3.依赖的JAR包的位置

暂无依赖的JAR包
