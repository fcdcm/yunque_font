package com.larkmidtable.yunque;

import com.larkmidtable.yunque.rpc.ServerServiceImpl;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import www.larkmidtable.com.exception.YunQueException;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 *
 * @Date: 2023/8/9 23:38
 * @Description:
 **/
public class YunQueEngineServer {
	private static final Logger logger = LoggerFactory.getLogger(YunQueEngineServer.class);

	private static ExecutorService pool = Executors.newCachedThreadPool();

	public static void main(String[] args) {

		if (args.length != 2) {
			String[] dargs = { "-port", "6060" };
			args = dargs;
			logger.info("您未指定服务端口[指定端口示例: -port 6060]，已为您分配系统默认端口" + args[1]);
		}
		BasicParser parser = new BasicParser();
		Options options = new Options();
		options.addOption("port", true, "服务器端口");
		CommandLine cl = null;
		try {
			cl = parser.parse(options, args);
		} catch (ParseException e) {
			e.printStackTrace();
			throw new YunQueException("需要传递 服务器端口 参数....", e);
		}
		Integer portNumber = Integer.parseInt(cl.getOptionValue("port"));
		//执行远程服务
		runRPCServer(new ServerServiceImpl(),portNumber);
	}

	/**
	 * 执行远程服务
	 * @param service 服务端实现类
	 */
	public static void runRPCServer(Object service,int port){

		ServerSocket server = null;
		try {
			server = new ServerSocket(port);
			System.out.println("YunQue后端服务启动，服务端口：" + port + "启动...");
			while(true){
				final Socket accept = server.accept();
				pool.submit(new Runnable() {
					public void run() {
						ObjectInputStream input = null;
						ObjectOutputStream output = null;
						try {
							// 从监听的socket中获得输入流
							input = new ObjectInputStream(accept.getInputStream());
							logger.debug("output = " + output);
							String methodName = input.readUTF();
							logger.debug("methodName = " + methodName);
							Class<?>[] parameterTypes = (Class<?>[]) input.readObject();
							logger.debug("parameterTypes = " + parameterTypes);
							//客户端代理中发送的代理方法的参数Object[]
							Object[] args = (Object[]) input.readObject();
							logger.debug("args = " + args);
							// 从监听的socket中获得输出流
							output = new ObjectOutputStream(accept.getOutputStream());
							Method method = service.getClass().getMethod(methodName, parameterTypes);
							Object back = method.invoke(service, args);
							output.writeObject(back);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}

		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			try {
				if(null != server){
					server.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
