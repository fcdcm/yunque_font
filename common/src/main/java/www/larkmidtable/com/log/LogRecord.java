package www.larkmidtable.com.log;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import www.larkmidtable.com.model.DbTaskResult;
import www.larkmidtable.com.model.TaskResult;
import www.larkmidtable.com.util.FunctionUtil;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

/**
 * 日志记录
 *
 * @author fei
 * @date 2023-07-05
 **/
@Getter
@Setter
public class LogRecord {
	private static Logger logger = LoggerFactory.getLogger(LogRecord.class);
	private static String datetimeFormat = "yyyy-MM-dd HH:mm:ss";

	private String groupId;
	private String taskId;
	private String taskMessage;
	private String threadName;
	private long startTime;
	private long endTime;
	private long duration;

	public static LogRecord newInstance() {
		return new LogRecord();
	}

	public void start(String taskMessage) {
		this.taskMessage = taskMessage;
		this.threadName = Thread.currentThread().getName();
		this.startTime = System.currentTimeMillis();
	}

	public void end() {
		logFormat(this.threadName,
				this.taskMessage,
				TaskResultFlag.Finish.getMessage(),
				this.startTime);
	}

	public void ok() {
		logFormat(this.threadName,
				this.taskMessage,
				TaskResultFlag.Success.getMessage(),
				this.startTime);
	}

	public void ok(String failInfo) {
		logFormat(this.threadName,
				this.taskMessage,
				TaskResultFlag.Success.getMessage(),
				this.startTime,
				failInfo);
	}

	public void fail() {
		logFormat(this.threadName,
				this.taskMessage,
				TaskResultFlag.Fail.getMessage(),
				this.startTime);
	}

	public void fail(String failInfo) {
		logFormat(this.threadName,
				this.taskMessage,
				TaskResultFlag.Fail.getMessage(),
				this.startTime,
				failInfo);
	}

	private static void logFormat(String threadName, String taskMessage, String taskResult, long startTime) {
		long endTime = System.currentTimeMillis();
		long duration = endTime - startTime;
		logger.info("线程名: [{}], [{}] 任务{}, 开始时间: [{}], 结束时间: [{}], 任务耗时 [{}] ms",
				threadName,
				taskMessage,
				taskResult,
				DateFormatUtils.format(startTime, datetimeFormat),
				DateFormatUtils.format(endTime, datetimeFormat),
				duration);
	}

	private static void logFormat(String threadName, String taskMessage, String taskResult, long startTime, String detailInfo) {
		long endTime = System.currentTimeMillis();
		long duration = endTime - startTime;
		logger.info("线程名: [{}], [{}] 任务{}, 开始时间: [{}], 结束时间: [{}], 任务耗时 [{}] ms\n{}",
				threadName,
				taskMessage,
				taskResult,
				DateFormatUtils.format(startTime, datetimeFormat),
				DateFormatUtils.format(endTime, datetimeFormat),
				duration,
				detailInfo);
	}

	public void totalStatistics(List<CompletableFuture<TaskResult>> resultFutures) {
		Optional<DbTaskResult> resultOptional = resultFutures.stream()
				.map(CompletableFuture::join)
				.map(taskResult -> (DbTaskResult) taskResult)
				.reduce(FunctionUtil.reduceResultFunc());
		if (resultOptional.isPresent()) {
			DbTaskResult dbTaskResult = resultOptional.get();
			if (dbTaskResult.isSuccess()) {
				ok(String.format("\t--> 任务统计：共 %s 条数据，最长任务执行耗时 %s ms",
						dbTaskResult.getSuccessRowNum(),
						dbTaskResult.getDuration()));
			} else {
				fail(String.format("\t--> 任务统计：成功 %s 条，失败 %s 条，最长任务执行耗时 %s ms",
						dbTaskResult.getSuccessRowNum(),
						dbTaskResult.getFailRowNum(),
						dbTaskResult.getDuration()));
			}
		} else {
			logger.warn("未读取到数据");
		}
	}

	private enum TaskResultFlag {
		Finish("结束"),
		Success("成功"),
		Fail("失败");

		private final String message;

		TaskResultFlag(String message) {
			this.message = message;
		}

		public String getMessage() {
			return this.message;
		}
	}
}
